local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")

-- Set variables
local configfile = "/etc/unbound/unbound.conf"
local processname = "unbound"
local packagename = "unbound"

local config

-- ################################################################################
-- LOCAL FUNCTIONS

local parseconfig = function()
	if config then return config end
	local temp = format.parse_linesandwords(fs.read_file(configfile) or "", "#")
	config = {}
	local cur = config
	for i,line in ipairs(temp) do
		local attr = string.match(line[1], "(.*):")
		if #line == 1 then
			config[attr] = {}
			cur = config[attr]
		else
			table.remove(line, 1)
			cur[attr] = table.concat(line, " ")
		end
	end
	return config
end

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "Unbound Status")
end

function mymodule.get_filedetails()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.update_filedetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.get_logfile(f)
	local retval = cfe({ type="structure", value={}, label="Unbound Log Files" })
	config = config or parseconfig()
	-- Determine the log file from the config file
	if config.server and config.server.logfile and config.server.logfile ~= "" then
		retval.value[#retval.value+1] = {filename=config.server.logfile}
	else
		-- report syslog even if use-syslog == "no"
		retval.value[#retval.value+1] = {facility="daemon", grep="unbound"}
	end
	return retval
end

return mymodule
