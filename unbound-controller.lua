local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit Unbound Config", "Configuration Set")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
